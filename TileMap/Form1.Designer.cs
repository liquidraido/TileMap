﻿namespace TileMap
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.W2TButton = new System.Windows.Forms.Button();
            this.zoomTextBox = new System.Windows.Forms.TextBox();
            this.yLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.latTextBox = new System.Windows.Forms.TextBox();
            this.xLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lonTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.T2WButton = new System.Windows.Forms.Button();
            this.t2wZoomTextBox = new System.Windows.Forms.TextBox();
            this.LatLabel = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tileYTextBox = new System.Windows.Forms.TextBox();
            this.lonLabel = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tileXTextBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.bottomTextBox = new System.Windows.Forms.TextBox();
            this.zoomAreaTextBox = new System.Windows.Forms.TextBox();
            this.rightTextBox = new System.Windows.Forms.TextBox();
            this.leftTextBox = new System.Windows.Forms.TextBox();
            this.topTextBox = new System.Windows.Forms.TextBox();
            this.downloadButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.W2TButton);
            this.groupBox1.Controls.Add(this.zoomTextBox);
            this.groupBox1.Controls.Add(this.yLabel);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.latTextBox);
            this.groupBox1.Controls.Add(this.xLabel);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lonTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(552, 296);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(225, 222);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "WorldToTile";
            // 
            // W2TButton
            // 
            this.W2TButton.Location = new System.Drawing.Point(66, 118);
            this.W2TButton.Name = "W2TButton";
            this.W2TButton.Size = new System.Drawing.Size(75, 23);
            this.W2TButton.TabIndex = 2;
            this.W2TButton.Text = "Calc";
            this.W2TButton.UseVisualStyleBackColor = true;
            this.W2TButton.Click += new System.EventHandler(this.W2TButton_Click);
            // 
            // zoomTextBox
            // 
            this.zoomTextBox.Location = new System.Drawing.Point(66, 91);
            this.zoomTextBox.Name = "zoomTextBox";
            this.zoomTextBox.Size = new System.Drawing.Size(100, 21);
            this.zoomTextBox.TabIndex = 1;
            this.zoomTextBox.Text = "15";
            // 
            // yLabel
            // 
            this.yLabel.AutoSize = true;
            this.yLabel.Location = new System.Drawing.Point(76, 180);
            this.yLabel.Name = "yLabel";
            this.yLabel.Size = new System.Drawing.Size(41, 12);
            this.yLabel.TabIndex = 0;
            this.yLabel.Text = "label1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "Zoom";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "Y";
            // 
            // latTextBox
            // 
            this.latTextBox.Location = new System.Drawing.Point(66, 64);
            this.latTextBox.Name = "latTextBox";
            this.latTextBox.Size = new System.Drawing.Size(100, 21);
            this.latTextBox.TabIndex = 1;
            // 
            // xLabel
            // 
            this.xLabel.AutoSize = true;
            this.xLabel.Location = new System.Drawing.Point(76, 153);
            this.xLabel.Name = "xLabel";
            this.xLabel.Size = new System.Drawing.Size(41, 12);
            this.xLabel.TabIndex = 0;
            this.xLabel.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "Lat";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "X";
            // 
            // lonTextBox
            // 
            this.lonTextBox.Location = new System.Drawing.Point(66, 37);
            this.lonTextBox.Name = "lonTextBox";
            this.lonTextBox.Size = new System.Drawing.Size(100, 21);
            this.lonTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lon";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.T2WButton);
            this.groupBox2.Controls.Add(this.t2wZoomTextBox);
            this.groupBox2.Controls.Add(this.LatLabel);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.tileYTextBox);
            this.groupBox2.Controls.Add(this.lonLabel);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.tileXTextBox);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Location = new System.Drawing.Point(783, 296);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(225, 222);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "TileToWorld";
            // 
            // T2WButton
            // 
            this.T2WButton.Location = new System.Drawing.Point(66, 118);
            this.T2WButton.Name = "T2WButton";
            this.T2WButton.Size = new System.Drawing.Size(75, 23);
            this.T2WButton.TabIndex = 2;
            this.T2WButton.Text = "Calc";
            this.T2WButton.UseVisualStyleBackColor = true;
            this.T2WButton.Click += new System.EventHandler(this.T2WButton_Click);
            // 
            // t2wZoomTextBox
            // 
            this.t2wZoomTextBox.Location = new System.Drawing.Point(66, 91);
            this.t2wZoomTextBox.Name = "t2wZoomTextBox";
            this.t2wZoomTextBox.Size = new System.Drawing.Size(100, 21);
            this.t2wZoomTextBox.TabIndex = 1;
            this.t2wZoomTextBox.Text = "15";
            // 
            // LatLabel
            // 
            this.LatLabel.AutoSize = true;
            this.LatLabel.Location = new System.Drawing.Point(76, 180);
            this.LatLabel.Name = "LatLabel";
            this.LatLabel.Size = new System.Drawing.Size(41, 12);
            this.LatLabel.TabIndex = 0;
            this.LatLabel.Text = "label1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(19, 94);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 12);
            this.label13.TabIndex = 0;
            this.label13.Text = "Zoom";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(19, 180);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 12);
            this.label14.TabIndex = 0;
            this.label14.Text = "Lat";
            // 
            // tileYTextBox
            // 
            this.tileYTextBox.Location = new System.Drawing.Point(66, 64);
            this.tileYTextBox.Name = "tileYTextBox";
            this.tileYTextBox.Size = new System.Drawing.Size(100, 21);
            this.tileYTextBox.TabIndex = 1;
            // 
            // lonLabel
            // 
            this.lonLabel.AutoSize = true;
            this.lonLabel.Location = new System.Drawing.Point(76, 153);
            this.lonLabel.Name = "lonLabel";
            this.lonLabel.Size = new System.Drawing.Size(41, 12);
            this.lonLabel.TabIndex = 0;
            this.lonLabel.Text = "label1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(19, 67);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 12);
            this.label16.TabIndex = 0;
            this.label16.Text = "TileY";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(19, 153);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(23, 12);
            this.label17.TabIndex = 0;
            this.label17.Text = "Lon";
            // 
            // tileXTextBox
            // 
            this.tileXTextBox.Location = new System.Drawing.Point(66, 37);
            this.tileXTextBox.Name = "tileXTextBox";
            this.tileXTextBox.Size = new System.Drawing.Size(100, 21);
            this.tileXTextBox.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(19, 40);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 12);
            this.label18.TabIndex = 0;
            this.label18.Text = "TileX";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(909, 39);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.bottomTextBox);
            this.groupBox3.Controls.Add(this.zoomAreaTextBox);
            this.groupBox3.Controls.Add(this.rightTextBox);
            this.groupBox3.Controls.Add(this.leftTextBox);
            this.groupBox3.Controls.Add(this.topTextBox);
            this.groupBox3.Controls.Add(this.downloadButton);
            this.groupBox3.Location = new System.Drawing.Point(552, 133);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(456, 157);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // bottomTextBox
            // 
            this.bottomTextBox.Location = new System.Drawing.Point(125, 117);
            this.bottomTextBox.Name = "bottomTextBox";
            this.bottomTextBox.Size = new System.Drawing.Size(100, 21);
            this.bottomTextBox.TabIndex = 1;
            this.bottomTextBox.Text = "34.30";
            // 
            // zoomAreaTextBox
            // 
            this.zoomAreaTextBox.Location = new System.Drawing.Point(332, 117);
            this.zoomAreaTextBox.Name = "zoomAreaTextBox";
            this.zoomAreaTextBox.Size = new System.Drawing.Size(100, 21);
            this.zoomAreaTextBox.TabIndex = 1;
            this.zoomAreaTextBox.Text = "12";
            // 
            // rightTextBox
            // 
            this.rightTextBox.Location = new System.Drawing.Point(231, 68);
            this.rightTextBox.Name = "rightTextBox";
            this.rightTextBox.Size = new System.Drawing.Size(100, 21);
            this.rightTextBox.TabIndex = 1;
            this.rightTextBox.Text = "108.8";
            // 
            // leftTextBox
            // 
            this.leftTextBox.Location = new System.Drawing.Point(17, 68);
            this.leftTextBox.Name = "leftTextBox";
            this.leftTextBox.Size = new System.Drawing.Size(100, 21);
            this.leftTextBox.TabIndex = 1;
            this.leftTextBox.Text = "108.632813";
            // 
            // topTextBox
            // 
            this.topTextBox.Location = new System.Drawing.Point(125, 20);
            this.topTextBox.Name = "topTextBox";
            this.topTextBox.Size = new System.Drawing.Size(100, 21);
            this.topTextBox.TabIndex = 1;
            this.topTextBox.Text = "34.37971";
            // 
            // downloadButton
            // 
            this.downloadButton.Location = new System.Drawing.Point(357, 32);
            this.downloadButton.Name = "downloadButton";
            this.downloadButton.Size = new System.Drawing.Size(75, 23);
            this.downloadButton.TabIndex = 0;
            this.downloadButton.Text = "DownLoad";
            this.downloadButton.UseVisualStyleBackColor = true;
            this.downloadButton.Click += new System.EventHandler(this.downloadButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1020, 530);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button W2TButton;
        private System.Windows.Forms.TextBox zoomTextBox;
        private System.Windows.Forms.Label yLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox latTextBox;
        private System.Windows.Forms.Label xLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox lonTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button T2WButton;
        private System.Windows.Forms.TextBox t2wZoomTextBox;
        private System.Windows.Forms.Label LatLabel;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tileYTextBox;
        private System.Windows.Forms.Label lonLabel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tileXTextBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox bottomTextBox;
        private System.Windows.Forms.TextBox rightTextBox;
        private System.Windows.Forms.TextBox leftTextBox;
        private System.Windows.Forms.TextBox topTextBox;
        private System.Windows.Forms.Button downloadButton;
        private System.Windows.Forms.TextBox zoomAreaTextBox;
    }
}

