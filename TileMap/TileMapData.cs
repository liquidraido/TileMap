﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileMap
{
    class TileMapCellData
    {
        public double Left { get; set; }
        public double Right { get; set; }
        public double Top { get; set; }
        public double Bottom { get; set; }

        public string TextureFileName { get; set; }
        public string HeightMapFileName { get; set; }

        /// <summary>
        /// 地图数据，2^n + 1
        /// </summary>
        public int HeightMapResolution { get; set; }

        public float MinHeight { get; set; }
        public float MaxHeight { get; set; }
    }

    [Serializable]
    class TileMapCellHeightData
    {
        public float[,] Data { get; set; }
    }

    [Serializable]
    class TileMapData
    {
        public double Left { get; set; }
        public double Right { get; set; }
        public double Top { get; set; }
        public double Bottom { get; set; }

        public double CellWidth { get; set; }
        public double CellHeight { get; set; }

        public int CellColumnCount { get; set; }
        public int CellRowCount { get; set; }

        public TileMapCellData[,] CellDatas { get; set; }

        public void SaveXML(string filename)
        {
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(this.GetType());
            using (var stream = System.IO.File.OpenWrite(filename))
            {
                xmlSerializer.Serialize(stream, this);
            }
        }

        public void SaveJson(string filename)
        {
            var val = Newtonsoft.Json.JsonConvert.SerializeObject(this,
                Newtonsoft.Json.Formatting.Indented);
            using (var stream = System.IO.File.CreateText(filename))
            {
                stream.Write(val);
            }
        }

        static public TileMapData LoadJson(string filename)
        {
            TileMapData res = null;
            if (File.Exists(filename))
            {
                using (var stream = File.OpenText(filename))
                {
                    var json = stream.ReadToEnd();
                    res = Newtonsoft.Json.JsonConvert.DeserializeObject<TileMapData>(json);
                }
            }
            return res;
        }
    }
}