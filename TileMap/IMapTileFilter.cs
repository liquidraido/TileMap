﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileMap
{
    public interface IMapTileFilter
    {
        int ColumnCount { get; set; }
        int RowCount { get; set; }

        void Filter(Image image, int row, int column);
    }
}
