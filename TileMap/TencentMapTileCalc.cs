﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileMap
{
    /// <summary>
    /// 经纬度和瓦片计算类
    /// 
    /// 依赖MapPosCalc计算。
    /// </summary>
    class TencentMapTileCalc
    {
        /// <summary>
        /// 腾讯地图下经纬度计算瓦片索引
        /// </summary>
        /// <param name="coord"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        static public Point WorldToTilePos(Coordinate coord, int zoom)
        {
            Coordinate tencent = new Coordinate()
            {
                Lat = -coord.Lat,
                Lon = coord.Lon
            };
            return MapPosCalc.WorldToTilePos(tencent, zoom);
        }

        /// <summary>
        /// 腾讯地图下瓦片索引计算经纬度
        /// </summary>
        /// <param name="tile"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        static public Coordinate TileToWorldPos(Point tile, int zoom)
        {
            Coordinate c = MapPosCalc.TileToWorldPos(tile, zoom);
            c.Lat *= -1;
            return c;
        }

        [Obsolete]
        static public PointF GetTile(Coordinate coord, int zoom)
        {
            var target = MapPosCalc.LonLat2Mercator(coord);
            var bl = MapPosCalc.LonLat2Mercator(new Coordinate(-180, -85.05112877980659));
            var tr = MapPosCalc.LonLat2Mercator(new Coordinate(180, 85.05112877980659));

            double w = (tr.Lon - bl.Lon) / Math.Pow(2, zoom);//格网宽度
            double h = (tr.Lat - bl.Lat) / Math.Pow(2, zoom);//格网高度

            double cd = ((target.Lon - bl.Lon) / w);
            double rd = ((target.Lat - bl.Lat) / h);
            int c = (int)((target.Lon - bl.Lon) / w);
            int r = (int)((target.Lat - bl.Lat) / h);
            return new PointF(c, r);
        }
    }
}
