﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileMap
{
    class Coordinate
    {
        public double Lon { get; set; }
        public double Lat { get; set; }

        public Coordinate()
        {
            Lon = 0;
            Lat = 0;
        }

        public Coordinate(double lon, double lat)
        {
            Lon = lon;
            Lat = lat;
        }

        public static Coordinate operator +(Coordinate a, Coordinate b)
        {
            a.Lon += b.Lon;
            a.Lat += b.Lat;
            return a;
        }
    }

}
