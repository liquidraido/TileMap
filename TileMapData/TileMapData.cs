﻿using System;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;

namespace TileMap
{
    public class TileMapCellData
    {
        public double Left { get; set; }
        public double Right { get; set; }
        public double Top { get; set; }
        public double Bottom { get; set; }

        public string TextureFileName { get; set; }
        public string HeightMapFileName { get; set; }

        /// <summary>
        /// 地图数据，2^n + 1
        /// </summary>
        public int HeightMapResolution { get; set; }

        public float MinHeight { get; set; }
        public float MaxHeight { get; set; }
    }

    [Serializable]
    public class TileMapCellHeightData
    {
        public float[,] Data { get; set; }

        public void Serialize(string fileName)
        {
            using (var f = new GZipStream(File.Create(fileName), CompressionMode.Compress))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(f, this);
            }
        }

        public static TileMapCellHeightData Deserialize(string fileName)
        {
            using (var r = new GZipStream(File.OpenRead(fileName), CompressionMode.Decompress))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                return (TileMapCellHeightData)formatter.Deserialize(r);
            }
        }
    }

    [Serializable]
    public class TileMapData
    {
        public double Left { get; set; }
        public double Right { get; set; }
        public double Top { get; set; }
        public double Bottom { get; set; }

        public double CellWidth { get; set; }
        public double CellHeight { get; set; }

        public int CellColumnCount { get; set; }
        public int CellRowCount { get; set; }

        public TileMapCellData[,] CellDatas { get; set; }

        public void SaveXML(string filename)
        {
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(this.GetType());
            using (var stream = System.IO.File.OpenWrite(filename))
            {
                xmlSerializer.Serialize(stream, this);
            }
        }

        public void SaveJson(string filename)
        {
            var val = Newtonsoft.Json.JsonConvert.SerializeObject(this,
                Newtonsoft.Json.Formatting.Indented);
            using (var stream = System.IO.File.CreateText(filename))
            {
                stream.Write(val);
            }
        }

        static public TileMapData LoadJson(string filename)
        {
            TileMapData res = null;
            if (File.Exists(filename))
            {
                using (var stream = File.OpenText(filename))
                {
                    var json = stream.ReadToEnd();
                    res = Newtonsoft.Json.JsonConvert.DeserializeObject<TileMapData>(json);
                }
            }
            return res;
        }
    }
}